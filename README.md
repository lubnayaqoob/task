# Coding challenge for QA

## Application to be tested:
**Carpark Ubi**

## Domain vocabulary:
EV - electric vehicle.
CP - charging point, an element in an infrastructure that supplies electric energy for the recharging of electric vehicles.

## Application details:
The given test subject is a simple backend application (with UI) to manage the charging points installed at Carpark Ubi.
The backend application exposes endpoints to Plug and Unplug an EV to a CP. It also exposes a report endpoint to view the current status of all CPs in Carpark Ubi.
The frontend application is a charge point management dashboard used by Support engineers to manage the CPs incase the Customer faces any difficulties e.g.: Connector is not unlocked. The UI also contains a view of current energy distribution.

## Logic behind the energy distribution
Carpark Ubi has 10 charging points installed. When a car is connected it consumes either 20 Amperes (fast charging) or 10 Amperes (slow charging). 
Carpark Ubi installation has an overall current input of 100 Amperes so it can support fast charging for a maximum of 5 cars or slow charging for a maximum of 10 cars at one time.
A charge point notifies the application when a car is plugged or unplugged.
The application must distribute the available current of 100 Amperes among the charging points so that when possible all cars use fast charging and when the current is not sufficient some cars are switched to slow charging. 
Cars which were connected earlier have lower priority than those which were connected later.
The application must also provide a report with a current state of each charging point returning a list of charging point, status (free or occupied) and - if occupied – the consumed current.

## Examples:

```
CP1 sends a notification that a car is plugged
Report: 
CP1 OCCUPIED 20A
CP2 AVAILABLE
...
CP10 AVAILABLE
```

```
CP1, CP2, CP3, CP4, CP5 and CP6 send notification that a car is plugged
Report:
CP1 OCCUPIED 10A
CP2 OCCUPIED 10A
CP3 OCCUPIED 20A
CP4 OCCUPIED 20A
CP5 OCCUPIED 20A
CP6 OCCUPIED 20A
CP7 AVAILABLE
...
CP10 AVAILABLE
```

## Test scope
Scope of the test challenge is this energy distribution logic.
Validate this logic from the UI perspective.

## Running the application

docker run -p 8080:8080 ubitricity/dummy-cpms-backend

If you are running the project directly from github repository run it using the below command with JDK 11 and maven
> mvn clean spring-boot:run

The application UI can be viewed using link http://localhost:8080/ (If your docker is not reachable via localhost then replace it with the IP)
The swagger specification can be found under http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

Rest API validation is not mandatory but including 1 sample test for this will be valued.

## Deliverables:
> Link to the git repository with the automation test suite testing the UI of this application.

> Test documentation written before implementing automation test suite in a README.md of the repository.
