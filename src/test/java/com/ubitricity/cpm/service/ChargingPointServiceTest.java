package com.ubitricity.cpm.service;

import com.ubitricity.cpm.data.ChargingPointRepo;
import com.ubitricity.cpm.exception.ChargingPointException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

@SpringBootTest
public class ChargingPointServiceTest {

    @BeforeTestMethod
    public void beforMethod() {

    }

    @MockBean
    private ChargingPointService service;

    @MockBean
    private ChargingPointRepo repo;

    @Test
    void initChargingPoints() {
    }

    @Test
    void findAll() {
    }

    @Test
    void plug() throws ChargingPointException {

    }

    @Test
    void unplug() {
    }
}
