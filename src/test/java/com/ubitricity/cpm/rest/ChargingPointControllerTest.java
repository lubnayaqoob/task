package com.ubitricity.cpm.rest;


import com.ubitricity.cpm.data.ChargingPoint;
import com.ubitricity.cpm.data.ChargingPointRepo;
import com.ubitricity.cpm.exception.ChargingPointException;
import com.ubitricity.cpm.service.ChargingPointService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ChargingPointControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ChargingPointService service;

    @MockBean
    private ChargingPointRepo repo;

    @Test
    public void shouldReturnChargingPointsWithHttpOKStatus() throws Exception {
        Mockito.when(service.findAll()).thenReturn(Arrays.asList(new ChargingPoint(1)));
        MvcResult result = this.mockMvc.perform(get("/cpm/dashboard")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        List<ChargingPoint> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ChargingPoint>>() {});
        AssertionErrors.assertEquals("Expected 1", actual.size() , 1);
    }

    @Test
    public void shouldPlugServiceFailWithProperHttpStatus() throws Exception {
        Mockito.when(repo.findById(ArgumentMatchers.anyLong())).thenAnswer(invocation -> { throw new ChargingPointException("some message"); });
        this.mockMvc.perform(get("/cpm/management/plug/1")).andExpect(status().is4xxClientError());
    }
}
