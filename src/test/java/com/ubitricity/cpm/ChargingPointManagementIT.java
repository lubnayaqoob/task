package com.ubitricity.cpm;

import com.ubitricity.cpm.data.ChargingPoint;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ChargingPointManagementIT {
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void afterEachMethod() throws Exception {
        this.mockMvc.perform(put("/cpm/management/unplug/1"));
        this.mockMvc.perform(put("/cpm/management/unplug/2"));
        this.mockMvc.perform(put("/cpm/management/unplug/3"));
        this.mockMvc.perform(put("/cpm/management/unplug/4"));
        this.mockMvc.perform(put("/cpm/management/unplug/5"));
        this.mockMvc.perform(put("/cpm/management/unplug/6"));
        this.mockMvc.perform(put("/cpm/management/unplug/7"));
        this.mockMvc.perform(put("/cpm/management/unplug/8"));
        this.mockMvc.perform(put("/cpm/management/unplug/9"));
        this.mockMvc.perform(put("/cpm/management/unplug/10"));
    }

    @Test
    public void shouldInitializeTenCPsDuringBootstrap() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/cpm/dashboard")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        List<ChargingPoint> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ChargingPoint>>() {
        });
        AssertionErrors.assertEquals("Expected 10", actual.size(), 10);
    }

    @Test
    public void shouldPlugCPAndAllocateMaxPower() throws Exception {
        this.mockMvc.perform(put("/cpm/management/plug/1")).andExpect(status().isOk()).andReturn();
        MvcResult result = this.mockMvc.perform(get("/cpm/dashboard")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        List<ChargingPoint> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ChargingPoint>>() {
        });
        AssertionErrors.assertEquals("Expected 10", actual.size(), 10);
        AssertionErrors.assertEquals("Expected True", actual.get(0).isOccupied(), true);
        AssertionErrors.assertEquals("Expected 20", actual.get(0).getPower(), 20);
        AssertionErrors.assertEquals("Expected false", actual.get(1).isOccupied(), false);
        AssertionErrors.assertEquals("Expected 0", actual.get(1).getPower(), 0);
        AssertionErrors.assertEquals("Expected false", actual.get(2).isOccupied(), false);
    }

    @Test
    public void shouldReDistributePowerWhenCPPluggedOrUnPlugged() throws Exception {
        this.mockMvc.perform(put("/cpm/management/plug/1")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/2")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/3")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/4")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/5")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/6")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/7")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/8")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/9")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/plug/10")).andExpect(status().isOk()).andReturn();
        MvcResult result = this.mockMvc.perform(get("/cpm/dashboard")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        List<ChargingPoint> actual = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ChargingPoint>>() {
        });
        IntStream.range(0, actual.size()).forEach( i -> {
            AssertionErrors.assertEquals("Expected True", actual.get(i).isOccupied(), true);
            AssertionErrors.assertEquals("Expected 20", actual.get(i).getPower(), 10);
        });
        this.mockMvc.perform(put("/cpm/management/unplug/1")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/unplug/3")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/unplug/5")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/unplug/7")).andExpect(status().isOk()).andReturn();
        this.mockMvc.perform(put("/cpm/management/unplug/9")).andExpect(status().isOk()).andReturn();

        MvcResult resultAfter = this.mockMvc.perform(get("/cpm/dashboard")).andExpect(status().isOk()).andReturn();
        ObjectMapper mapperAfter = new ObjectMapper();
        List<ChargingPoint> actualAfter = mapperAfter.readValue(resultAfter.getResponse().getContentAsString(), new TypeReference<List<ChargingPoint>>() {
        });
        Arrays.asList(0, 2, 4, 6, 8).forEach(i -> {
            AssertionErrors.assertEquals("Expected True", actualAfter.get(i).isOccupied(), false);
            AssertionErrors.assertEquals("Expected 20", actualAfter.get(i).getPower(), 0);
        });
        Arrays.asList(1, 3, 5, 7, 9).forEach(i -> {
            AssertionErrors.assertEquals("Expected True", actualAfter.get(i).isOccupied(), true);
            AssertionErrors.assertEquals("Expected 20", actualAfter.get(i).getPower(), 20);
        });

    }



}
