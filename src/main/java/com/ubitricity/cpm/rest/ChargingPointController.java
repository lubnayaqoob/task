package com.ubitricity.cpm.rest;
import com.ubitricity.cpm.data.ChargingPoint;
import com.ubitricity.cpm.exception.ChargingPointException;
import com.ubitricity.cpm.service.ChargingPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This class act as main REST endpoint for this service
 * This is responsible only for REST endpoints, the business logic is abstracted out in service.
 */
@RestController
@RequestMapping("/cpm")
public class ChargingPointController {
    @Autowired
    private ChargingPointService cpService;

    /**
     * Method return List of Charging Points with its occupency status power and time Charging Started
     * @return List of Charging Points
     */
    @CrossOrigin(origins = {"*"})
    @GetMapping("/dashboard")
    public Iterable<ChargingPoint> getChargingPoints() {
        return cpService.findAll();
    }

    /**
     * This method expose a PUT endpoint to mark a Charging Point is plugged
     * @param cpId
     * @return
     */
    @CrossOrigin(origins = {"*"})
    @PutMapping("/management/plug/{cpId}")
    public ResponseEntity<ChargingPoint> plug(@PathVariable("cpId") Long cpId) {
        try {
            new ResponseEntity(cpService.plug(cpId), HttpStatus.OK);
        }catch (ChargingPointException e){
            return new ResponseEntity(e.getMessage(), HttpStatus.CONFLICT);
        }
        return null;
    }

    /**
     * This method expose a PUT endpoint to unplug a Charging Point
     * @param cpId
     * @return
     */
    @CrossOrigin(origins = {"*"})
    @PutMapping("/management/unplug/{cpId}")
    public ResponseEntity<ChargingPoint> unplug(@PathVariable("cpId") Long cpId) {
        try {
            new ResponseEntity(cpService.unplug(cpId), HttpStatus.OK);
        }catch (ChargingPointException e){
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return null;
    }
}
