package com.ubitricity.cpm.data;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * This class handles Charging point data handling
 */
public interface ChargingPointRepo extends CrudRepository<ChargingPoint, Long> {
    List<ChargingPoint> findByOrderByConnectedTimeAsc();
    List<ChargingPoint> findByOrderByConnectedTimeDesc();
}
