package com.ubitricity.cpm.data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * POJO class which holds Charging Point Data
 */
@Entity(name = "CHARGING_POINT")
public class ChargingPoint {


    @Id
    private long id;
    private long connectedTime;
    private int power;
    private boolean occupied;

    public ChargingPoint() {
    }

    public ChargingPoint(long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConnectedTime() {
        return connectedTime;
    }

    public void setConnectedTime(long connectedTime) {
        this.connectedTime = connectedTime;
    }

    public int getPower() {
        return power;
    }
    public void setPower(int power) {
        this.power = power;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public boolean isOccupied() {
        return occupied;
    }
}
