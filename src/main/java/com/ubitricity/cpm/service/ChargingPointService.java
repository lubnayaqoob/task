package com.ubitricity.cpm.service;

import com.ubitricity.cpm.data.ChargingPoint;
import com.ubitricity.cpm.data.ChargingPointRepo;
import com.ubitricity.cpm.exception.ChargingPointException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.LongStream;

/**
 * This class provide all Charging Point Services.
 * Having a Service class helps to abstract the business logic from REST controller
 * This service assumes, charging points are aware to the service upfront.
 */
@Service
public class ChargingPointService {

    @Value("${charging.point.max.power}")
    private int MAX_POWER_PER_CP;

    @Value("${charging.point.min.power}")
    private int MIN_POWER_PER_CP;

    @Value("${charging.point.available.power}")
    private int availablePower;

    @Value("${charging.point.id.not.available}")
    private String CP_NOT_AVAILABLE;

    @Value("${charging.point.id.not.in.use}")
    private String CP_NOT_IN_USE;

    @Value("${charging.point.available.cps}")
    private int AVAILABLE_CPS = 10;

    @Value("${charging.point.id.not.found}")
    private String NO_CP_ID_FOUND;

    @Autowired
    private ChargingPointRepo cpRepo;


    /**
     * During the service bootstrap initialize available charging station
     * TODO: Is there a requirement to add / remove Charging Points? This behaviour is a product decision.
     */
    @PostConstruct
    public void initChargingPoints() {
        if(((Collection<?>)cpRepo.findAll()).isEmpty()) {//Initialize with available charging points
            final List<ChargingPoint> cps = new ArrayList<>(AVAILABLE_CPS);
            LongStream.range(1, AVAILABLE_CPS + 1).forEach(count -> cps.add(new ChargingPoint(count)));
            cpRepo.saveAll(cps);
        }
    }

    /**
     * Method return all Charging Points
     * @return
     */
    public Iterable<ChargingPoint> findAll() {
        return cpRepo.findAll();
    }

    /**
     * This method will mark a Charging Point Connection as Occupied and distribute consumed power accordingly
     * Distribution logic: More power to new connection and less for older connections
     * TODO: What if earlier connections were connected recently?
     * @param cpId
     * @return
     * @throws ChargingPointException
     */
    public synchronized ChargingPoint plug(final Long cpId) throws ChargingPointException {
        final ChargingPoint plugToCp = cpRepo.findById(cpId).orElseThrow(() -> new ChargingPointException(String.format(NO_CP_ID_FOUND, cpId)));
        if(plugToCp.isOccupied()) {//Charging Point is already in use
            throw new ChargingPointException(String.format(CP_NOT_AVAILABLE, cpId));
        }
        distributePowerAfterAddingNewConsumer(plugToCp);
        return plugToCp;
    }

    /**
     * This method will release /unplug a Charging Point and distribute released power accordingly
     * @param cpId
     * @return
     * @throws ChargingPointException
     */
    public synchronized ChargingPoint unplug(Long cpId) throws ChargingPointException {
        final ChargingPoint unplugFromCp = cpRepo.findById(cpId).orElseThrow(() -> new ChargingPointException(String.format(NO_CP_ID_FOUND, cpId)));
        if(unplugFromCp.isOccupied()) {//If not occupied, why to release?
            unplugFromCp.setOccupied(false);
            int power = unplugFromCp.getPower();
            unplugFromCp.setPower(0);
            unplugFromCp.setConnectedTime(0L);
            cpRepo.save(unplugFromCp);
            distributeReleasedPower(power);
        }else {
            throw new ChargingPointException(String.format(CP_NOT_IN_USE, cpId));
        }
        return unplugFromCp;
    }

    /**
     * When a car unplug from Charging Point, there will be excess power which this method will re-distribute to other connected cars
     * @param power - power released
     */
    private void distributeReleasedPower(final int power) {
        int remainingPower = power;
        for(ChargingPoint cp : cpRepo.findByOrderByConnectedTimeDesc()) {
            if(cp.isOccupied()) {
                final int powerToAdd = MAX_POWER_PER_CP - cp.getPower();//20 - 10 = 10 (or) 20 - 20 = 0
                if(powerToAdd != 0) {//
                    if (powerToAdd == remainingPower || powerToAdd > remainingPower) { //10 == 10 or 20 > 10
                        cp.setPower(cp.getPower() + remainingPower);
                        cpRepo.save(cp);
                        remainingPower = 0;
                    } else { //10 < 20
                        remainingPower = remainingPower - powerToAdd;
                        cp.setPower(cp.getPower() + remainingPower); // 20 - 10
                        cpRepo.save(cp);
                    }
                    if (remainingPower == 0) {//No more power to distribute
                        break;
                    }
                }
            }
        }
        availablePower += remainingPower;//if there are no CP to add power, add to availablePower
    }

    /**
     * When a new car plugged into a Charging Point, trye to allocate MAX power for that if possible.
     * First we will try to get it from available free power.
     * If there is no available power, earlier connected Charging Point power will be reduced from MAX to MIN and provide to new connection
     * @param plugToCp
     */
    private void distributePowerAfterAddingNewConsumer(final ChargingPoint plugToCp) {
        int newCpPower = 0;
        if (availablePower < MAX_POWER_PER_CP) {//If we do not have MAX_POWER_PER_CP power give to new cp, get it from existing MAX_POWER_PER_CP CPs
            for (ChargingPoint cp : cpRepo.findByOrderByConnectedTimeAsc()) {
                if(cp.isOccupied()) {
                    if (cp.getPower() == MAX_POWER_PER_CP) {
                        cp.setPower(MIN_POWER_PER_CP);//Reduce earlier connected cp power to MIN_POWER_PER_CP
                        cpRepo.save(cp);
                        newCpPower += MIN_POWER_PER_CP;//And allocate more power to new CP
                        if (newCpPower == MAX_POWER_PER_CP) {//That is MAX_POWER_PER_CP for a new cp power
                            break;
                        }
                    }
                }
            }
            if(newCpPower == 0) {//If no cps in MAX_POWER_PER_CP, allocate MIN_POWER_PER_CP to new
                newCpPower = MIN_POWER_PER_CP;
            }
        }else {//If there is >= MAX_POWER_PER_CP, allot the same to new cp
            newCpPower = MAX_POWER_PER_CP;
        }
        if(availablePower > 0) {
            availablePower = availablePower - newCpPower;//Reduce available power
        }
        plugToCp.setOccupied(true);
        plugToCp.setConnectedTime(System.currentTimeMillis());
        plugToCp.setPower(newCpPower);
        cpRepo.save(plugToCp);
    }
}
