package com.ubitricity.cpm.exception;

/**
 * Custom Exception class to handle Charging Point handle exceptions
 */
public class ChargingPointException extends Exception {
    public ChargingPointException(String message) {
        super(message);
    }
}
